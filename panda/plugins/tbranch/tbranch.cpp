/* PANDABEGINCOMMENT
 *
 * Authors:
 *  Tim Leek               tleek@ll.mit.edu
 *  Ryan Whelan            rwhelan@ll.mit.edu
 *  Joshua Hodosh          josh.hodosh@ll.mit.edu
 *  Michael Zhivich        mzhivich@ll.mit.edu
 *  Brendan Dolan-Gavitt   brendandg@gatech.edu
 *
 * This work is licensed under the terms of the GNU GPL, version 2.
 * See the COPYING file in the top-level directory.
 *
PANDAENDCOMMENT */
// This needs to be defined before anything is included in order to get
// the PRIx64 macro
#define __STDC_FORMAT_MACROS

#include <cstdio>
#include <vector>

#include "panda/plugin.h"
#include "panda/plugin_plugin.h"

#include "taint2/label_set.h"
#include "taint2/taint2.h"

extern "C" {
#include "panda/rr/rr_log.h"

#include "taint2/taint2_ext.h"
}

// NB: callstack_instr_ext needs this, sadly
#include "callstack_instr/callstack_instr.h"
#include "callstack_instr/callstack_instr_ext.h"

// this includes on_branch2_t
#include "taint2/taint2.h"


// These need to be extern "C" so that the ABI is compatible with
// QEMU/PANDA, which is written in C
extern "C" {

bool init_plugin(void *);
void uninit_plugin(void *);

#include <stdint.h>

}

int counter = 0;

#ifdef CONFIG_SOFTMMU

#include <map>
#include <set>

char *out;
FILE *fp_out;
bool debug = false;

void tbranch_on_branch_taint2(Addr a, uint64_t size) {
    assert (a.typ == LADDR);
    uint32_t num_tainted = 0;
    Addr ao = a;
    for (uint32_t o=0; o<size; o++) {
        ao.off = o;
        num_tainted += (taint2_query(ao) != 0);
    }
    if (num_tainted > 0) {
        for (uint32_t i = 0; i < size; i++ ){
            a.off = i;
            uint32_t label_count = taint2_query(a);
            std::vector<uint32_t> labels(label_count);
            taint2_query_set(a, labels.data());
            if (label_count > 0) {
                counter ++ ;
                if (debug) fprintf(stderr,"[debug] tainted_branch: instr=%" PRIu64 " label=[", rr_get_guest_instr_count());
                for (int j = 0; j < label_count; j++){
                    fprintf(fp_out,"%u,", labels[j]);
                    if(debug) fprintf(stderr,"%u,", labels[j]);
                }
                fprintf(fp_out,"\n");
                if (debug) fprintf(stderr,"]\n");
            }
        }
    }
}



#endif

bool init_plugin(void *self) {
    panda_require("callstack_instr");
    assert (init_callstack_instr_api());
    panda_require("taint2");
    assert (init_taint2_api());
    panda_arg_list *args = panda_get_args("tbranch");
    out = (char*)panda_parse_string(args,"out","tb.txt");
    debug = panda_parse_bool(args,"debug");
    PPP_REG_CB("taint2", on_branch2, tbranch_on_branch_taint2);
    fp_out = fopen(out, "w");
    return true;
}


void uninit_plugin(void *self) {
    fflush(fp_out);
    fclose(fp_out);
}
